Tento repozitář obsahuje jednoduchý CMakeLists.txt a několik C++ zdrojáků,
které ověří zdali je vaše prostředí připravené na kompilaci C++ v rámci PJC.

Prozatím to znamená
* Funkční podpora C++14 (pár náhodných konstruktů)
* Zdali fungují `std::thread` a ostatní nástroje pro práci s více vlákny
* Zdali se zkompiluje volání `std::to_string`
