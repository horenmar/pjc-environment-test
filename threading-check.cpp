#include <future>
#include <iostream>

static constexpr const int iters = 100;

int main() {
    auto a = std::async(std::launch::async, [](){
        int sum = 0;
        for (int i = 0; i < iters; ++i) {
            sum = i * i;
        }
        return sum;
    });
    auto b = std::async(std::launch::async, [](){
        int sum = 0;
        for (int i = 0; i < iters; ++i) {
            sum = i * i;
        }
        return sum;
    });
    auto c = std::async(std::launch::async, [](){
        int sum = 0;
        for (int i = 0; i < iters; ++i) {
            sum = i * i;
        }
        return sum;
    });
    auto d = std::async(std::launch::async, [](){
        int sum = 0;
        for (int i = 0; i < iters; ++i) {
            sum = i * i;
        }
        return sum;
    });
    std::cout << a.get() + b.get() + c.get() + d.get() << '\n';
}
